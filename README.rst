.. image:: https://travis-ci.org/lwm/csq.svg
    :target: https://travis-ci.org/lwm/csq

.. image:: https://codecov.io/github/lwm/csq/coverage.svg?branch=master
    :target: https://codecov.io/github/lwm/csq?branch=master

.. image:: https://badge.fury.io/py/csq.svg
    :target: https://badge.fury.io/py/csq

.. image:: https://img.shields.io/badge/license-GPLv3-brightgreen.svg
    :target: https://raw.githubusercontent.com/lwm/csq/master/LICENSE

.. image:: https://img.shields.io/badge/python-3.5-brightgreen.svg
    :target: https://github.com/lwm/csq/blob/master/tox.ini

csq: computing quotes on the command line
=========================================

~1000 quotes from over 30 humans in computing.

.. figure:: https://i.imgur.com/lAeN8vy.gif
   :alt: GIF


Installation
------------

Using `pip <https://docs.pipenv.org/>`__, it's easy:

::

    $ pip install csq


Contributing
------------

If you find some good quotes, feel free to `make a pull request <https://github.com/lwm/csq/pulls>`__,
you can find the current list of quotes `here <https://github.com/lwm/csq/blob/master/csq/quotes.json>`__.

You can get hacking by installing `pipenv <https://docs.pipenv.org/>` and running:

::

    $ pipenv install --dev --three

And then running your commands prefixed with:

::

    $ pipenv run

Or drop into the environment with:

::

    $ pipenv shell
