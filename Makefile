install:
	pipenv install --dev --three

test:
	pipenv run tox -v

release:
	python setup.py sdist upload -r pypi
