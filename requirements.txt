coverage==4.5.1
docopt==0.6.2
pluggy==0.3.1
py==1.5.2
pytest==2.8.3
pytest-cov==2.2.1
tox==2.2.1
virtualenv==15.1.0
