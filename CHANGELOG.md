# Change Log

All notable changes to this project will be documented in this file. The
format is based on [Keep a Changelog](http://keepachangelog.com/) and this
project adheres to [Semantic Versioning](http://semver.org/).

# [2.0.0]
- Deprecate Python 2.X.
- Add pipenv support for development.

# [1.0.7]
# [1.0.6]
# [1.0.5]
# [1.0.4]
# [1.0.3]
- Experimenting with pypi version bumping. No functionality changes.

# [1.0.2]
- Remove development dependencies from release. Please see [1e66905](https://github.com/lwm/csq/commit/1e66905099da9bab30397c2ad2b67492953487aa).

# [1.0.1]

### Added
- Add Doug McIlroy quotes. See [#4](https://github.com/lwm/csq/pull/4).
- Add Toby Radloff quote. See [#3](https://github.com/lwm/csq/pull/3).

### Fixed
- Fix unicode outputting error. See [#1](https://github.com/lwm/csq/issues/1).

# [1.0.0]
- Initial release
