import io
import os
import subprocess
import collections

from csq import csq, handler, VERSION


def test_indent():
    text = 'indent me'
    expected_length = len(text) + 4
    result = csq.indent(text)
    assert len(result) == expected_length


def test_flatten_quotes():
    quote_set = collections.OrderedDict((('a', [1, 2, 3]), ('b', [4, 5])))
    result = csq.flatten_quotes(quote_set)
    assert result == [(1, 'a'), (2, 'a'), (3, 'a'), (4, 'b'), (5, 'b')]


def test_load_quotes():
    fpath = os.path.join(csq.BASE_DIR, 'quotes.json')
    with io.open(fpath, encoding='utf-8') as fhandle:
        result = csq.load_quotes(fhandle)
        assert isinstance(result, dict)


def test_docopt_string():
    args = ['csq']
    r1 = subprocess.check_output(args).decode()
    assert handler.__doc__.strip() == r1.strip()


def test_docopt_string_with_long_help_arg():
    args = ['csq', '--help']
    r1 = subprocess.check_output(args).decode()
    assert handler.__doc__.strip() == r1.strip()


def test_long_version():
    args = ['csq', '--version']
    r1 = subprocess.check_output(args).decode()
    assert r1.strip() == VERSION


def test_short_version():
    args = ['csq', '-v']
    r1 = subprocess.check_output(args).decode()
    assert r1.strip() == VERSION


def test_quote():
    args = ['csq', 'quote']
    for _ in range(1, 5):
        assert 0 == subprocess.call(args)
