import io
import os
import json
import random
import textwrap

OFFSET = 65
INDENT_OFFSET = 4
BASE_DIR = os.path.abspath(os.path.dirname(__file__))


def indent(text):
    """Left indented text by length 'INDENT_OFFSET'."""
    return '\n'.join((' '*INDENT_OFFSET) + line for line in text.splitlines())


def format_name(name, quote_len):
    """Offset and indented name of person."""
    name = '|>> ' + name + ' <<|'
    offset = quote_len if quote_len < OFFSET else OFFSET
    return indent('\n' + name.rjust(offset))


def format_quote(quote):
    """Line wrapped, offset and indented quote."""
    wrapped_quote = '\n'.join(textwrap.wrap(quote, OFFSET))
    return indent(wrapped_quote)


def flatten_quotes(quote_set):
    """Transforms a dict of {k:[v], ...} to a list of [(k,v), ...]."""
    return [
        (quote, person)
        for person in quote_set
        for quote in quote_set[person]
    ]


def load_quotes(fhandle):
    """A JSON file handle with all the quotes."""
    return json.load(fhandle)


def quote():
    """A quote and the name of the person who said it."""
    fpath = os.path.join(BASE_DIR, 'quotes.json')
    with io.open(fpath, encoding='utf-8') as fhandle:
        loaded_quotes = load_quotes(fhandle)
    quote, name = random.choice(flatten_quotes(loaded_quotes))
    quote, name = format_quote(quote), format_name(name, len(quote))
    return quote, name
