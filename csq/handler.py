"""
Usage:
    csq quote
    csq [--version]

Arguments:
  quote             A computing quote

Options:
  -h --help         Show help
  -v --version      Show version
"""

import sys
import docopt

from csq import VERSION
from csq import csq


def handler():
    """CLI entry point to the program."""
    args = docopt.docopt(__doc__, argv=sys.argv[1:])

    if args['quote']:

        quote, name = csq.quote()
        print(quote, name)
    elif args['--version']:
        print(VERSION)
    else:
        print(__doc__)
