from setuptools import find_packages, setup
from csq import VERSION


setup(
    name='csq',
    version=VERSION,
    author='Luke Murphy',
    author_email='lukewm@riseup.net',
    description='Computing quotes on the command line',
    license='GPL',
    keywords='quote, computing, history, cli',
    url='https://github.com/lwm/csq',
    packages=find_packages(exclude='tests'),
    long_description=open('README.rst').read(),
    install_requires=['docopt'],
    entry_points={
        'console_scripts': ['csq=csq.handler:handler']
    },
    classifiers=[
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3.5',
        'License :: OSI Approved :: GNU General Public License (GPL)'
    ],
    package_data={
        'csq': ['quotes.json'],
    },
)
